# **VSCloud**

_Install VS Code on a remote machine accessible through a web browser_

Runs on [OpenVSCode Server](https://github.com/gitpod-io/openvscode-server) 

[Docker](https://github.com/chiefmikey/vscloud/tree/main/docker)

[Linux](https://github.com/chiefmikey/vscloud/tree/main/linux)

