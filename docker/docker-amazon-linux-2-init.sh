#!/bin/sh

INSTANCE_ALREADY_STARTED="INSTANCE_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e $INSTANCE_ALREADY_STARTED ]; then
sudo touch $INSTANCE_ALREADY_STARTED
  echo "-- First instance startup --"
  sudo yum update -y
  sudo amazon-linux-extras install docker
  sudo service docker start
  sudo usermod -a -G docker ec2-user
  sudo chkconfig docker on
  echo $(docker info)
  docker run -d --init -p 3000:3000 -v "$(pwd):/home/workspace:cached" gitpod/openvscode-server
else
  echo "-- Not first instance startup --"
  sudo yum update -y
  if [ "$(cat /home/ec2-user/vscloud/stop.txt)" = stop ]; then
    docker rm $(docker ps --filter status=exited -q)
  fi
  docker run -d --init -p 3000:3000 -v "$(pwd):/home/workspace:cached" gitpod/openvscode-server
fi







