#!/bin/sh

INSTANCE_ALREADY_STARTED="INSTANCE_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e ~/$INSTANCE_ALREADY_STARTED ]; then
sudo touch ~/$INSTANCE_ALREADY_STARTED
  echo "-- First instance startup --"
  sudo apt update -y
  sudo apt upgrade -y
  export SERVER_VERSION=1.60.2
  sudo wget https://github.com/gitpod-io/openvscode-server/releases/download/openvscode-server-v$SERVER_VERSION/openvscode-server-v$SERVER_VERSION-linux-x64.tar.gz -O /home/ubuntu/code-server.tar.gz
  sudo tar -xzf /home/ubuntu/code-server.tar.gz
  sudo rm /home/ubuntu/code-server.tar.gz
  sudo nohup /home/ubuntu/openvscode-server-v$SERVER_VERSION-linux-x64/server.sh &
else
  echo "-- Not first instance startup --"
  sudo apt update -y
  sudo apt upgrade -y
  if [ "$(cat /home/ubuntu/vscloud/stop.txt)" = stop ]; then
    sudo killall
  fi
  sudo nohup /home/ubuntu/openvscode-server-v$SERVER_VERSION-linux-x64/server.sh &
fi
